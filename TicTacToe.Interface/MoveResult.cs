﻿namespace TicTacToe.Interface
{
    /// <summary>
    /// Struktura reprezentująca wynik rucha gracza.
    /// </summary>
    public struct MoveResult
    {
        public Game Game { get; set; }
        public Player Player { get; set; }
        public Position Position { get; set; }
    }
}
