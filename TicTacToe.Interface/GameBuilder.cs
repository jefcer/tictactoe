﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Interface;

namespace TicTacToe.Interface
{
    public class GameBuilder
    {
        public int Rounds { get; private set; } = 3;
        public int MoveTimeLimit { get; private set; } = 10;
        public PawnType HostPawn { get; private set; } = PawnType.Cross;
        public StartPawnType StartingPawn { get; private set; } = StartPawnType.Random;
        public int MinimumWins { get; private set; } = 0;
        public bool DrawCountsAsWin { get; private set; } = false;
        public IBoard Board { get; private set; } = new NormalBoard();

        //public Game Build()
        //{
        //    return new Game(rounds, minimumWins, hostPawn, startingPawn, moveTimeLimit, drawCountsAsWin, board);
        //}

        /// <summary>
        /// Nadpisuje pola obietku GameBuilder
        /// </summary>
        /// <returns>
        /// Zwraca typ GameBuilder
        /// </returns>

        public GameBuilder UseDrawCountsAsWin(bool doCounts)
        {
            DrawCountsAsWin = doCounts;
            return this;
        }

        public GameBuilder UseHosterPawn(PawnType pawnType)
        {
            HostPawn = pawnType;
            return this;
        }

        public GameBuilder UseMinimumWins(int minimumWins)
        {
            MinimumWins = minimumWins;
            return this;
        }

        public GameBuilder UseMoveTimeLimit(int timeLimit)
        {
            MoveTimeLimit = timeLimit;
            return this;
        }

        public GameBuilder UseRounds(int roundNumber)
        {
            Rounds = roundNumber;
            return this;
        }

        public GameBuilder UseStartingPawn(StartPawnType startPawnType)
        {
            StartingPawn = startPawnType;
            return this;
        }

        public GameBuilder UseBoard(IBoard board)
        {
            Board = board;
            return this;
        }
    }
}
