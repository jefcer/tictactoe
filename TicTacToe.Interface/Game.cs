using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Klasa opisuj�ca gr�.
    /// </summary>
    public class Game
    {
        public int Rounds { get; private set; }
        public int MoveTimeLimit { get; private set; }
        public PawnType HostPawn { get; private set; }
        public StartPawnType StartingPawn { get; private set; }
        public int MinimumWins { get; private set; }
        public bool DrawCountsAsWin { get; private set; }
        [JsonConverter(typeof(BoardConverter))]
        public IBoard Board { get; private set; }

        public string Guid { get; set; }
        public Player? Host { get; set; }
        public Player? Guest { get; set; }
        public int HostScore { get; set; }
        public int GuestScore { get; set; }
        public int ActiveRound { get; set; }
        public GameResult GameResult { get; set; }
        public List<Position> CheckedPositions { get; set; }

        public Game(GameBuilder gameBuilder)
        {
            Rounds = gameBuilder.Rounds;
            MinimumWins = gameBuilder.MinimumWins;
            HostPawn = gameBuilder.HostPawn;
            StartingPawn = gameBuilder.StartingPawn;
            MoveTimeLimit = gameBuilder.MoveTimeLimit;
            DrawCountsAsWin = gameBuilder.DrawCountsAsWin;
            Board = gameBuilder.Board;
        }

        [JsonConstructor]
        private Game(int rounds, int minimumWins, PawnType hostPawn, StartPawnType startingPawn, int moveTimeLimit, bool drawCountsAsWin, IBoard board)
        {
            Rounds = rounds;
            MinimumWins = minimumWins;
            HostPawn = hostPawn;
            StartingPawn = startingPawn;
            MoveTimeLimit = moveTimeLimit;
            DrawCountsAsWin = drawCountsAsWin;
            Board = board;
        }

        /// <summary>
        /// Pobiera pozycje iteratora
        /// </summary>
        /// <returns>
        /// Zwraca nowy obiekt PositionIterator
        /// </returns>
        public IPositionIterator GetPositionIterator() 
        {
            if (CheckedPositions == null)
            {
                CheckedPositions = new List<Position>();
            }
            return new PositionIterator(CheckedPositions.ToArray(), Board.Size);
        }
    }
}
