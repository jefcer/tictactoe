﻿using System;
using TicTacToe.Interface;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Klasa reprezentujaca duzą planszę.
    /// </summary>
    public class BigBoard : IBoard
    {
        public int Size { get; private set; }

        public BigBoard()
        {
            Size = 4;
        }
    }
}
