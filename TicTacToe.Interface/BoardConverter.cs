﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Klasa służąca do konwertowania planszy.
    /// </summary>
    public class BoardConverter : JsonConverter
    {
        public override bool CanWrite => true;
        public override bool CanRead => true;

        /// <summary>
        /// Sprawdza czy można konwertować planszę.
        /// </summary>
        /// <returns>
        /// Wynik czy można konwertować planszę.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(IBoard);
        }

        /// <summary>
        /// Zapisywanie w formacie Json.
        /// </summary>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken t = JToken.FromObject(value);

            if (t.Type != JTokenType.Object)
            {
                t.WriteTo(writer);
            }
            else
            {
                JObject o = (JObject)t;

                o.Add("BoardType", new JValue(value.GetType().ToString()));

                o.WriteTo(writer);
            }
        }

        /// <summary>
        /// Czytanie z formatu Json.
        /// </summary>
        /// <returns>
        /// Plansza.
        /// </returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            IBoard board = null;
            var type = Type.GetType(jsonObject["BoardType"].Value<string>());
            if (type == typeof(LargeBoard))
            {
                board = new LargeBoard();
            }
            else if (type == typeof(BigBoard))
            {
                board = new BigBoard();
            }
            else if (type == typeof(NormalBoard))
            {
                board = new NormalBoard();
            }

            serializer.Populate(jsonObject.CreateReader(), board);
            return board;
        }
    }
}
