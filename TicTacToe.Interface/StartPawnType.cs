namespace TicTacToe.Interface
{
    /// <summary>
    /// Rodzaje pionka
    /// </summary>
    public enum StartPawnType
    {
        Nought,
        Cross,
        Random
    }
}
