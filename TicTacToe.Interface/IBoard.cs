﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Interfejs planszy.
    /// </summary>
    public interface IBoard
    {
        int Size { get; }
       
    }
}
