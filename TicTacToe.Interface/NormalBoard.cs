﻿using System;
using TicTacToe.Interface;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Klasa reprezentujaca normalną plansze
    /// </summary>
    public class NormalBoard : IBoard
    {
        public int Size { get; private set; }

        public NormalBoard()
        {
            Size = 3;
        }
    }
}
