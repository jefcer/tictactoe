﻿using System;
using TicTacToe.Interface;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Klasa reprezentujaca wielka plansze
    /// </summary>
    public class LargeBoard : IBoard
    {
        public int Size { get; private set; }

        public LargeBoard()
        {
            Size = 5;
        }
    }
}
