namespace TicTacToe.Interface
{
    /// <summary>
    /// Rodzaje pionka
    /// </summary>
    public enum PawnType
    {
        Nought,
        Cross
    }
}
