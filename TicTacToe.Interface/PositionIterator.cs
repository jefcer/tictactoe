﻿using System;
using System.Linq;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Wzorzec iterator
    /// </summary>
    public class PositionIterator : IPositionIterator
    {
        private Position[] positions;
        private int index;
        private int boardSize;

        public PositionIterator(Position[] positions, int boardSize)
        {
            index = 0;
            this.positions = positions;
            this.boardSize = boardSize;
        }

        /// <summary>
        /// Metoda sprawdza czy index jest mniejszy od rozmiaru planszy aby iterator nie wyszedl poza plansze
        /// </summary>
        /// <returns>
        /// Zwraca true albo false
        /// </returns>
        public bool HasNext()
        {
            return index < boardSize * boardSize;
        }

        /// <summary>
        /// Zwieksza index iteratora
        /// </summary>
        /// <returns>
        /// Zwraca nastepny element planszy
        /// </returns>
        public Position Next()
        {
            var x = index % boardSize;
            var y = index / boardSize;

            index++;

            return positions.FirstOrDefault(p => p.X == x && p.Y == y);
        }
    }
}
