﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Struktura przechowująca dane gracza.
    /// </summary>
    public struct Player
    {
        public string Username { get; set; }
        public string Guid { get; set; }
        public int Wins { get; set; }
    }
}
