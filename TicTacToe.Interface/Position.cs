﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Interface
{
    /// <summary>
    /// Klasa reprezentujaca wspolrzedne pionka oraz jego typ
    /// </summary>
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
        public PawnType Pawn { get; set; }
    }
}
