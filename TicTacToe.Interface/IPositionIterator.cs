﻿namespace TicTacToe.Interface
{
    /// <summary>
    /// Interfejs iteratora
    /// </summary>
    public interface IPositionIterator
    {
        bool HasNext();
        Position Next();
    }
}
