using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TicTacToe.Interface;

namespace TicTacToe.Server
{
    public abstract class GameServerHub : Hub // Template Method
    {
        protected abstract MoveResult Move(Game game, Player player, Position position);
        protected abstract bool Join(Game game, Player player);
        protected abstract bool CancelGameCreation(Game game);
        protected abstract bool AddGame(Game game);
        protected abstract IList<Game> FindPlayableGames();
        protected abstract void Connected(string username, string guid);
        protected abstract void Disconnected(string username, string guid);

        public void MakeMove(string guid, Game game, Player player, Position position)
        {
            var oldResult = game.GameResult;
            var result = Move(game, player, position);
            Clients.All.MakeMoveResult(guid, result);
            Console.WriteLine("Make move for player {0}", guid);
            if (result.Game.GameResult != oldResult)
            {
                var moveResult = result;
                if (guid == game.Host.Value.Guid)
                {
                    Console.WriteLine("Host made move at {0}:{1}", position.X, position.Y);
                    Clients.All.PlayerMadeMove(game.Guest.Value.Guid, moveResult);
                }
                else
                {
                    Console.WriteLine("Guest made move at {0}:{1}", position.X, position.Y);
                    Clients.All.PlayerMadeMove(game.Host.Value.Guid, moveResult);
                }
            }
        }

        public void JoinGame(string guid, Game game, Player player)
        {
            game.Guest = player;
            var result = Join(game, player);
            if (result)
            {
                Clients.All.JoinGameResult(guid, game);
                Clients.All.PlayerJoinedGame(game.Host.Value.Guid, game);
            }
        }

        public void CancelGame(string guid, Game game)
        {
            var result = CancelGameCreation(game);
            Clients.All.CancelGameCreationResult(guid, result);
        }

        public void CreateGame(string guid, Game game)
        {
            game.Guid = Guid.NewGuid().ToString();
            var result = AddGame(game);
            if (result)
            {
                Clients.All.GameCreationResult(guid, game);
            }
            else
            {
                Clients.All.GameCreationResult(guid, default(Game));
            }
        }

        public void GetPlayableServers(string guid)
        {
            var servers = FindPlayableGames();
            Clients.All.PlayableServersResult(guid, servers);
        }

        public override Task OnConnected()
        {
            var username = Context.QueryString["username"];
            var guid = Context.QueryString["guid"];
            Connected(username, guid);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var username = Context.QueryString["username"];
            var guid = Context.QueryString["guid"];
            Disconnected(username, guid);

            return base.OnDisconnected(stopCalled);
        }
    }
}
