using Microsoft.Owin.Hosting;
using System;
using System.Configuration;
using System.Net;
using System.Reflection;

namespace TicTacToe.Server
{
    class Program
    {
        static IDisposable SignalR;

        static void Main(string[] args)
        {
            ServicePointManager.DefaultConnectionLimit = 50;
            var serverUrl = ConfigurationManager.AppSettings["Url"];

            try
            {
                SignalR = WebApp.Start<Startup>(serverUrl);
            }
            catch (TargetInvocationException)
            {
                CloseApplication(string.Format("A server is already running at {0}", serverUrl));
            }
            Console.WriteLine("Server started at " + serverUrl);
            Console.WriteLine("Press an Escape key to close application...");
            
            while (true)
            {
                ConsoleKeyInfo ki = Console.ReadKey(true);
                if (ki.Key == ConsoleKey.Escape)
                {
                    break;
                }
            }
        }

        static void CloseApplication(string message)
        {
            Console.WriteLine(message);
            Console.WriteLine("Press any key to close application...");
            Console.ReadKey(true);
            if (SignalR != null)
            {
                SignalR.Dispose();
            }
            Environment.Exit(0);
        }
    }
}
