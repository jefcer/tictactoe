using System;
using System.Collections.Generic;
using System.Linq;
using TicTacToe.Interface;

namespace TicTacToe.Server
{
    public class GameServer : GameServerHub
    {
        private static IList<Game> games = new List<Game>();

        protected override bool AddGame(Game game)
        {
            if (games.Where(g => g.Host.Value.Guid == game.Host.Value.Guid).Any())
            {
                return false;
            }

            games.Add(game);
            Console.WriteLine("Username {0} (GUID: {1}) has created game (GUID: {2})", 
                game.Host.Value.Username, game.Host.Value.Guid, game.Guid);
            return true;
        }

        protected override bool CancelGameCreation(Game game)
        {
            var savedGame = games.FirstOrDefault(g => g.Guid == game.Guid);
            if (savedGame != null)
            {
                Console.WriteLine("Username {0} (GUID: {1}) has removed game (GUID: {2})",
                    savedGame.Host.Value.Username, savedGame.Host.Value.Guid, savedGame.Guid);
                games.Remove(savedGame);
            }
            return true;
        }

        protected override void Connected(string username, string guid)
        {
            Console.WriteLine("Username {0} (GUID: {1}) has been connected to the server", username, guid);
        }

        protected override void Disconnected(string username, string guid)
        {
            Console.WriteLine("Username {0} (GUID: {1}) has been disconnected from the server", username, guid);
        }

        protected override IList<Game> FindPlayableGames()
        {
            return games.Where(g => g.Guest == null).ToList();
        }

        protected override bool Join(Game game, Player player)
        {
            game = games.Where(g => g.Guid == game.Guid).First();
            if (player.Wins >= game.MinimumWins)
            {
                Console.WriteLine("Username {0} (GUID: {1}) has joined the game (GUID: {2})",
                    player.Username, player.Guid, game.Guid);

                game.HostScore = 0;
                game.GuestScore = 0;
                game.ActiveRound = 1;
                if (game.StartingPawn == StartPawnType.Cross)
                {
                    game.GameResult = GameResult.CrossMove;
                }
                else if (game.StartingPawn == StartPawnType.Nought)
                {
                    game.GameResult = GameResult.NoughtMove;
                }
                else
                {
                    Random rnd = new Random();
                    if (rnd.Next(100) % 2 == 0)
                    {
                        game.GameResult = GameResult.CrossMove;
                    }
                    else
                    {
                        game.GameResult = GameResult.NoughtMove;
                    }
                }
                game.Guest = player;
                return true;
            }
            return false;
        }

        protected override MoveResult Move(Game game, Player player, Position position)
        {
            game = games.Where(g => g.Guid == game.Guid).First();
            var positions = game.GetPositionIterator();
            while (positions.HasNext())
            {
                var pos = positions.Next();
                if (pos != null && pos.X == position.X && pos.Y == position.Y)
                {
                    return new MoveResult { Game = game, Player = player, Position = position };
                }
            }
            
            game.CheckedPositions.Add(position);

            GameResult GetResult()
            {
                var xLine = new int[game.Board.Size];
                var yLine = new int[game.Board.Size];
                var leftCross = 0;
                var rightCross = 0;
                positions = game.GetPositionIterator();
                var count = 0;
                while (positions.HasNext())
                {
                    var pos = positions.Next();
                    if (pos != null)
                    {
                        count++;
                    }
                    if (pos != null && pos.Pawn == position.Pawn)
                    {
                        if (pos.X == pos.Y)
                        {
                            leftCross++;
                        }
                        else if ((pos.Y > pos.X && game.Board.Size - pos.Y == pos.X + 1) || (pos.X > pos.Y && game.Board.Size - pos.X == pos.Y + 1) || (game.Board.Size % 2 != 0 && pos.X == game.Board.Size / 2 && pos.Y == pos.X))
                        {
                            rightCross++;
                        }

                        xLine[pos.X]++;
                        yLine[pos.Y]++;
                    }
                }

                if (count == (game.Board.Size * game.Board.Size))
                {
                    return GetWinner(ref game, ref player, ref position, true);
                }
                foreach (var line in xLine)
                {
                    if (line == game.Board.Size)
                    {
                        return GetWinner(ref game, ref player, ref position, false);
                    }
                }
                foreach (var line in yLine)
                {
                    if (line == game.Board.Size)
                    {
                        return GetWinner(ref game, ref player, ref position, false);
                    }
                }
                if (leftCross == game.Board.Size)
                {
                    return GetWinner(ref game, ref player, ref position, false);
                }
                if (rightCross == game.Board.Size)
                {
                    return GetWinner(ref game, ref player, ref position, false);
                }

                return position.Pawn == PawnType.Cross ? GameResult.NoughtMove : GameResult.CrossMove;
            }

            game.GameResult = GetResult();
            if (game.GameResult == GameResult.CrossWonRound || game.GameResult == GameResult.NoughtWonRound || game.GameResult == GameResult.RoundDraw)
            {
                game.CheckedPositions.Clear();
            }
            else if (game.GameResult == GameResult.NoughtWonGame || game.GameResult == GameResult.CrossWonGame || game.GameResult == GameResult.GameDraw)
            {
                games.Remove(games.First(g => g.Guid == game.Guid));
            }
            return new MoveResult { Game = game, Player = player, Position = position };
        }

        private GameResult GetWinner(ref Game game, ref Player player, ref Position position, bool isDraw)
        {
            if (isDraw)
            {
                game.HostScore++;
                game.GuestScore++;
                if (game.HostScore > game.Rounds && game.HostScore > game.GuestScore)
                {
                    var host = game.Host.Value;
                    host.Wins++;
                    game.Host = host;
                    return game.HostPawn == PawnType.Cross ? GameResult.CrossWonGame : GameResult.NoughtWonGame;
                }
                else if (game.GuestScore > game.Rounds && game.HostScore < game.GuestScore)
                {
                    var guest = game.Guest.Value;
                    guest.Wins++;
                    game.Guest = guest;
                    return game.HostPawn == PawnType.Cross ? GameResult.NoughtWonGame : GameResult.CrossWonGame;
                }
                else if (game.HostScore > game.Rounds && game.GuestScore > game.Rounds)
                {
                    if (game.DrawCountsAsWin)
                    {
                        var host = game.Host.Value;
                        host.Wins++;
                        game.Host = host;
                        var guest = game.Guest.Value;
                        guest.Wins++;
                        game.Guest = guest;
                    }
                    return GameResult.GameDraw;
                }
                return GameResult.RoundDraw;
            }
            else if (game.HostPawn == position.Pawn)
            {
                game.HostScore++;
            }
            else
            {
                game.GuestScore++;
            }

            if (game.HostScore > game.Rounds || game.GuestScore > game.Rounds)
            {
                if (game.HostScore > game.GuestScore)
                {
                    var host = game.Host.Value;
                    host.Wins++;
                    game.Host = host;
                    return game.HostPawn == PawnType.Cross ? GameResult.CrossWonGame : GameResult.NoughtWonGame;
                }
                else if (game.HostScore < game.GuestScore)
                {
                    var guest = game.Guest.Value;
                    guest.Wins++;
                    game.Guest = guest;
                    return game.HostPawn == PawnType.Cross ? GameResult.NoughtWonGame : GameResult.CrossWonGame;
                }
                else
                {
                    if (game.DrawCountsAsWin)
                    {
                        var host = game.Host.Value;
                        host.Wins++;
                        game.Host = host;
                        var guest = game.Guest.Value;
                        guest.Wins++;
                        game.Guest = guest;
                    }

                    return GameResult.GameDraw;
                }
            }
            else
            {
                return position.Pawn == PawnType.Cross ? GameResult.CrossWonRound : GameResult.NoughtWonRound;
            }
        }
    }
}
