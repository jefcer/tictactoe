﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TicTacToe.Interface;
using TicTacToe.Utilities;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for MediumBoardGame.xaml
    /// </summary>
    public partial class MediumBoardGame : Window
    {
        private Position position;
        private PawnType yourPawn;
        private int _yourPoints;
        private int _opponentPoints;
        private int yourPoints
        {
            set
            {
                _yourPoints = value;
                FirstPointsTxtbox.Text = _yourPoints.ToString();
            }
            get
            {
                return _yourPoints;
            }
        }
        private int opponentPoints
        {
            set
            {
                _opponentPoints = value;
                SecondPointsTxtbox.Text = _opponentPoints.ToString();
            }
            get
            {
                return _opponentPoints;
            }
        }

        public MediumBoardGame()
        {
            InitializeComponent();

            FirstUserNameLbl.Content = StaticHelper.ActivePlayer.Username;
            if (StaticHelper.Game.Host.Value.Guid == StaticHelper.ActivePlayer.Guid)
            {
                SecondUserNameLbl.Content = StaticHelper.Game.Guest.Value.Username;
            }
            else
            {
                SecondUserNameLbl.Content = StaticHelper.Game.Host.Value.Username;
            }

            yourPoints = 0;
            opponentPoints = 0;

            if (StaticHelper.Game.Guest.Value.Guid == StaticHelper.ActivePlayer.Guid)
            {
                yourPawn = StaticHelper.Game.HostPawn == PawnType.Cross ? PawnType.Nought : PawnType.Cross;
            }
            else
            {
                yourPawn = StaticHelper.Game.HostPawn;
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            resetBoard((yourPawn == PawnType.Cross && StaticHelper.Game.GameResult == GameResult.CrossMove) || (yourPawn == PawnType.Nought && StaticHelper.Game.GameResult == GameResult.NoughtMove));
        }

        private void Btn00_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 0, Y = 0 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn00.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn01_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 1, Y = 0 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn01.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn02_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 2, Y = 0 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn02.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn03_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 3, Y = 0 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn03.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn10_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 0, Y = 1 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn10.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn11_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 1, Y = 1 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn11.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn12_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 2, Y = 1 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn12.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn13_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 3, Y = 1 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn13.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn20_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 0, Y = 2 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn20.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn21_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 1, Y = 2 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn21.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn22_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 2, Y = 2 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn22.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn23_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 3, Y = 2 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn23.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn30_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 0, Y = 3 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn30.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn31_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 1, Y = 3 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn31.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn32_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 2, Y = 3 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn32.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void Btn33_Click(object sender, RoutedEventArgs e)
        {
            position = new Position { Pawn = yourPawn, X = 3, Y = 3 };
            if (StaticHelper.GameManager.Move(ref StaticHelper.Game, ref StaticHelper.ActivePlayer, ref position))
            {
                Btn33.Content = yourPawn == PawnType.Cross ? "X" : "O";
                parseGameResult();
            }
        }

        private void parseGameResult()
        {
            switch (StaticHelper.Game.GameResult)
            {
                case GameResult.CrossWonGame:
                    if (yourPawn == PawnType.Cross)
                    {
                        StaticHelper.ActivePlayer.Wins++;
                        MessageBox.Show("You have won the game!\nTry to win again", "Congratulations!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("You have lost the game!\nTry to be better next time", "Oops!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    Close();
                    break;
                case GameResult.NoughtWonGame:
                    if (yourPawn == PawnType.Nought)
                    {
                        StaticHelper.ActivePlayer.Wins++;
                        MessageBox.Show("You have won the game!\nTry to win again", "Congratulations!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("You have lost the game!\nTry to be better next time", "Oops!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    Close();
                    break;
                case GameResult.GameDraw:
                    if (StaticHelper.Game.DrawCountsAsWin)
                    {
                        StaticHelper.ActivePlayer.Wins++;
                        MessageBox.Show("Both of you have won the game!\nTry to win again", "Congratulations!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("None of you have won the game!\nTry to be better next time", "Draw!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    Close();
                    break;
                case GameResult.CrossWonRound:
                    if (yourPawn == PawnType.Cross)
                    {
                        resetBoard(false);
                    }
                    else
                    {
                        resetBoard(true);
                    }
                    break;
                case GameResult.NoughtWonRound:
                    if (yourPawn == PawnType.Cross)
                    {
                        resetBoard(true);
                    }
                    else
                    {
                        resetBoard(false);
                    }
                    break;
                case GameResult.RoundDraw:
                    if ((yourPawn == PawnType.Cross && StaticHelper.Game.StartingPawn == StartPawnType.Cross)
                        || (yourPawn == PawnType.Nought && StaticHelper.Game.StartingPawn == StartPawnType.Nought)
                        || (yourPawn == PawnType.Cross && StaticHelper.Game.StartingPawn == StartPawnType.Random))
                    {
                        resetBoard(true);
                    }
                    else
                    {
                        resetBoard(false);
                    }
                    break;
                case GameResult.CrossMove:
                    if (yourPawn != PawnType.Cross)
                    {
                        waitForMove();
                    }
                    break;
                case GameResult.NoughtMove:
                    if (yourPawn != PawnType.Nought)
                    {
                        waitForMove();
                    }
                    break;
            }

            yourPoints = StaticHelper.Game.Host.Value.Guid == StaticHelper.ActivePlayer.Guid ? StaticHelper.Game.HostScore : StaticHelper.Game.GuestScore;
            opponentPoints = StaticHelper.Game.Host.Value.Guid == StaticHelper.ActivePlayer.Guid ? StaticHelper.Game.GuestScore : StaticHelper.Game.HostScore;
        }

        private void drawOtherPlayerMove(Position position)
        {
            if (position.X == 0 && position.Y == 0)
            {
                Btn00.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 1 && position.Y == 0)
            {
                Btn01.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 2 && position.Y == 0)
            {
                Btn02.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 3 && position.Y == 0)
            {
                Btn03.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 0 && position.Y == 1)
            {
                Btn10.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 1 && position.Y == 1)
            {
                Btn11.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 2 && position.Y == 1)
            {
                Btn12.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 3 && position.Y == 1)
            {
                Btn13.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 0 && position.Y == 2)
            {
                Btn20.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 1 && position.Y == 2)
            {
                Btn21.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 2 && position.Y == 2)
            {
                Btn22.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 3 && position.Y == 2)
            {
                Btn23.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 0 && position.Y == 3)
            {
                Btn30.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 1 && position.Y == 3)
            {
                Btn31.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 2 && position.Y == 3)
            {
                Btn32.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
            else if (position.X == 3 && position.Y == 3)
            {
                Btn33.Content = position.Pawn == PawnType.Cross ? "X" : "O";
            }
        }

        private void resetBoard(bool isYourTurn)
        {
            Btn00.Content = "";
            Btn01.Content = "";
            Btn02.Content = "";
            Btn03.Content = "";
            Btn10.Content = "";
            Btn11.Content = "";
            Btn12.Content = "";
            Btn13.Content = "";
            Btn20.Content = "";
            Btn21.Content = "";
            Btn22.Content = "";
            Btn23.Content = "";
            Btn30.Content = "";
            Btn31.Content = "";
            Btn32.Content = "";
            Btn33.Content = "";

            if (!isYourTurn)
            {
                var connectingWindow = new InfoWindow("Waiting for other player move...");
                connectingWindow.ContentRendered += (ea, a) =>
                {
                    try
                    {
                        StaticHelper.GameManager.GetOtherPlayerMove(ref StaticHelper.Game, ref position);
                        drawOtherPlayerMove(position);
                    }
                    catch (AggregateException)
                    {
                        MessageBox.Show("Couldn't connect to the server\nTry to restart your application", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        Close();
                    }
                    finally
                    {
                        connectingWindow.Close();
                    }
                };
                connectingWindow.ShowDialog();
            }
        }

        private void waitForMove()
        {
            var connectingWindow = new InfoWindow("Waiting for other player move...");
            connectingWindow.ContentRendered += (ea, a) =>
            {
                try
                {
                    StaticHelper.GameManager.GetOtherPlayerMove(ref StaticHelper.Game, ref position);
                    drawOtherPlayerMove(position);
                    parseGameResult();
                }
                catch (AggregateException)
                {
                    MessageBox.Show("Couldn't connect to the server\nTry to restart your application", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    Close();
                }
                finally
                {
                    connectingWindow.Close();
                }
            };
            connectingWindow.ShowDialog();
        }
    }
}
