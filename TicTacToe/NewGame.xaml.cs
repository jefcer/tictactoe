﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TicTacToe.Interface;
using TicTacToe.Utilities;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for NewGame.xaml
    /// </summary>
    public partial class NewGame : Window
    {
        private IList<Game> games;

        public NewGame()
        {
            InitializeComponent();
            Color c = ProgramSettings.Instance.BackgroundColor;
            Background = new SolidColorBrush(c);
        }

        private void FindServerBtn_Click(object sender, RoutedEventArgs e)
        {
            games = StaticHelper.GameManager.FindPlayableGames();
            ServerGrid.Items.Clear();
            foreach (var game in games)
            {
                ServerGrid.Items.Add(game);
            }

            if (games.Count == 0)
            {
                MessageBoxResult result = MessageBox.Show("No games found\nYou can create one", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void CreateGameBtn_Click(object sender, RoutedEventArgs e)
        {
            var createGame = new CreateGame();
            if (createGame.ShowDialog().Value)
            {
                var connectingWindow = new InfoWindow("Waiting for player to join...");
                connectingWindow.ContentRendered += (ea, a) =>
                {
                    try
                    {
                        StaticHelper.GameManager.AddGame(ref StaticHelper.Game);

                        if (StaticHelper.Game.Board is NormalBoard)
                        {
                            var smallBoardGame = new SmallBoardGame();
                            smallBoardGame.Closed += (ea2, a2) =>
                            {
                                var profileManager = new ProfilesManager();
                                profileManager.UpdateProfile(new Profile { Player = StaticHelper.ActivePlayer, ProgramSettings = ProgramSettings.Instance });
                                Show();
                            };
                            smallBoardGame.Show();
                            Hide();
                        }
                        else if (StaticHelper.Game.Board is BigBoard)
                        {
                            var mediumBoardGame = new MediumBoardGame();
                            mediumBoardGame.Closed += (ea2, a2) =>
                            {
                                var profileManager = new ProfilesManager();
                                profileManager.UpdateProfile(new Profile { Player = StaticHelper.ActivePlayer, ProgramSettings = ProgramSettings.Instance });
                                Show();
                            };
                            mediumBoardGame.Show();
                            Hide();
                        }
                        else
                        {
                            var bigBoardGame = new BigBoardGame();
                            bigBoardGame.Closed += (ea2, a2) =>
                            {
                                var profileManager = new ProfilesManager();
                                profileManager.UpdateProfile(new Profile { Player = StaticHelper.ActivePlayer, ProgramSettings = ProgramSettings.Instance });
                                Show();
                            };
                            bigBoardGame.Show();
                            Hide();
                        }
                    }
                    catch (AggregateException)
                    {
                        MessageBoxResult result = MessageBox.Show("Couldn't connect to the server\nTry to restart your application", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        Close();
                    }
                    finally
                    {
                        connectingWindow.Close();
                    }
                };
                connectingWindow.ShowDialog();
            }
        }

        private void ConnectBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectedGame = (Game)ServerGrid.SelectedItem;
            if (selectedGame.MinimumWins > StaticHelper.ActivePlayer.Wins)
            {
                MessageBoxResult result = MessageBox.Show("Couldn't join the game\nYour number of wins is too low", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                var connectingWindow = new InfoWindow("Joining game...");
                connectingWindow.ContentRendered += (ea, a) =>
                {
                    try
                    {
                        StaticHelper.Game = selectedGame;
                        StaticHelper.GameManager.Join(ref StaticHelper.Game, ref StaticHelper.ActivePlayer);

                        if (StaticHelper.Game.Board is NormalBoard)
                        {
                            var smallBoardGame = new SmallBoardGame();
                            smallBoardGame.Closed += (ea2, a2) =>
                            {
                                var profileManager = new ProfilesManager();
                                profileManager.UpdateProfile(new Profile { Player = StaticHelper.ActivePlayer, ProgramSettings = ProgramSettings.Instance });
                                Show();
                            };
                            smallBoardGame.Show();
                            Hide();
                        }
                        else if (StaticHelper.Game.Board is BigBoard)
                        {
                            var mediumBoardGame = new MediumBoardGame();
                            mediumBoardGame.Closed += (ea2, a2) =>
                            {
                                var profileManager = new ProfilesManager();
                                profileManager.UpdateProfile(new Profile { Player = StaticHelper.ActivePlayer, ProgramSettings = ProgramSettings.Instance });
                                Show();
                            };
                            mediumBoardGame.Show();
                            Hide();
                        }
                        else
                        {
                            var bigBoardGame = new BigBoardGame();
                            bigBoardGame.Closed += (ea2, a2) =>
                            {
                                var profileManager = new ProfilesManager();
                                profileManager.UpdateProfile(new Profile { Player = StaticHelper.ActivePlayer, ProgramSettings = ProgramSettings.Instance });
                                Show();
                            };
                            bigBoardGame.Show();
                            Hide();
                        }
                    }
                    catch (AggregateException)
                    {
                        MessageBoxResult result = MessageBox.Show("Couldn't connect to the server\nTry to restart your application", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        Close();
                    }
                    finally
                    {
                        connectingWindow.Close();
                    }
                };
                connectingWindow.ShowDialog();
            }
        }
    }
}
