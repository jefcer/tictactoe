﻿using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Wzorzec Fabryka
    /// </summary>
    public class BoardFactory : IBoardFactory
    {
        /// <summary>
        /// Pobiera obiekt klasy NormalBoard.
        /// </summary>
        /// <returns>
        /// Obiekt klasy NormalBoard.
        /// </returns>
        public IBoard GetNormalBoard()
        {
            return new NormalBoard();
        }

        /// <summary>
        /// Pobiera obiekt klasy BigBoard.
        /// </summary>
        /// <returns>
        /// Obiekt klasy BigBoard.
        /// </returns>
        public IBoard GetBigBoard()
        {
            return new BigBoard();
        }

        /// <summary>
        /// Pobiera obiekt klasy LargeBoard.
        /// </summary>
        /// <returns>
        /// Obiekt klasy LargeBoard.
        /// </returns>
        public IBoard GetLargeBoard()
        {
            return new LargeBoard();
        }
    }
}
