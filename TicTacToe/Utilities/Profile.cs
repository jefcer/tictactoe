﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Klasa przechowująca gracza i ustawienia gry.
    /// </summary>
    public class Profile
    {
        public Player Player { get; set; }
        public ProgramSettings ProgramSettings { get; set; }
    }
}
