﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Interfejs klasy ProfieSaver.
    /// </summary>
    public interface IProfileSaver
    {
        void Save(String filePath);
        string ParseToFile();
    }
}
