﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Serwer gry.
    /// Zawiera metody do osbsługi serwera.
    /// </summary>
    public class GameServer : IGameServer
    {
        private IHubProxy hubProxy;
        private const string ServerURI = "http://localhost:8080/signalr"; // TODO: load it from settings
        private HubConnection connection;
        private string username;
        private string guid;
        private int reconnectDelay = 1; // TODO: load it from settings
        private bool isReverseGame;

        private Game createGameResult;
        private bool createGameResultReceived = false;
        private Game joinGameResult;
        private bool joinGameResultReceived = false;
        private Game playerJoinedGame;
        private bool playerJoinedGameReceived = false;
        private bool cancelGameCreationResult;
        private bool cancelGameCreationResultReceived = false;
        private IList<Game> playableServersResult;
        private bool playableServersResultReceived = false;
        private MoveResult makeMoveResult;
        private bool makeMoveResultReceived = false;
        private MoveResult playerMadeMoveResult;
        private bool playerMadeMoveResultReceived = false;

        /// <summary>
        /// Łączy się z serwerem automatycznie.
        /// </summary>
        public GameServer(string username, string guid, bool isReverseGame)
        {
            this.username = username;
            this.guid = guid;
            this.isReverseGame = isReverseGame;
            Connect(username, guid, isReverseGame);
        }

        /// <summary>
        /// Łączenie się z serwerem.
        /// </summary>
        private void Connect(string username, string guid, bool isReverseGame)
        {
            ServicePointManager.DefaultConnectionLimit = 20;
            var querystringData = new Dictionary<string, string>();
            querystringData.Add("UserName", username);
            querystringData.Add("GUID", guid);
            connection = new HubConnection(ServerURI, querystringData);
            if (isReverseGame)
            {
                hubProxy = connection.CreateHubProxy("ReverseGameServer");
            }
            else
            {
                hubProxy = connection.CreateHubProxy("GameServer");
            }
            hubProxy.On<string, Game>("GameCreationResult", (guidResult, result) =>
            {
                if (guidResult == guid)
                {
                    createGameResult = result;
                    createGameResultReceived = true;
                }
            });
            hubProxy.On<string, Game>("JoinGameResult", (guidResult, result) =>
            {
                if (guidResult == guid)
                {
                    joinGameResult = result;
                    joinGameResultReceived = true;
                }
            });
            hubProxy.On<string, Game>("PlayerJoinedGame", (guidResult, result) =>
            {
                if (guidResult == guid)
                {
                    playerJoinedGame = result;
                    playerJoinedGameReceived = true;
                }
            });
            hubProxy.On<string, bool>("CancelGameCreationResult", (guidResult, result) =>
            {
                if (guidResult == guid)
                {
                    cancelGameCreationResult = result;
                    cancelGameCreationResultReceived = true;
                }
            });
            hubProxy.On<string, IList<Game>>("PlayableServersResult", (guidResult, result) =>
            {
                if (guidResult == guid)
                {
                    playableServersResult = result;
                    playableServersResultReceived = true;
                }
            });
            hubProxy.On<string, MoveResult>("MakeMoveResult", (guidResult, result) =>
            {
                if (guidResult == guid)
                {
                    makeMoveResult = result;
                    makeMoveResultReceived = true;
                }
            });
            hubProxy.On<string, MoveResult>("PlayerMadeMove", (guidResult, result) =>
            {
                if (guidResult == guid)
                {
                    playerMadeMoveResult = result;
                    playerMadeMoveResultReceived = true;
                }
            });

            connection.Start().Wait();
        }

        /// <summary>
        /// Przerwanie połączenia z serwerem.
        /// </summary>
        public void Dispose()
        {
            if (connection != null)
            {
                connection.Stop();
                connection.Dispose();
            }
        }

        /// <summary>
        /// Anulowanie gry.
        /// </summary>
        public void CancelGame(Game game)
        {
            //hubProxy.Invoke("CancelGame", game);
            int count = 0;
            while (!hubProxy.Invoke("CancelGame", guid, game).Wait(1000))
            {
                count++;
                if (count == reconnectDelay)
                {
                    Dispose();
                    Connect(username, guid, isReverseGame);
                }
            }
        }

        /// <summary>
        /// Tworzenie gry.
        /// </summary>
        public void CreateGame(Game game)
        {
            //hubProxy.Invoke("CreateGame", game);
            int count = 0;
            while (!hubProxy.Invoke("CreateGame", guid, game).Wait(1000))
            {
                count++;
                if (count == reconnectDelay)
                {
                    Dispose();
                    Connect(username, guid, isReverseGame);
                }
            }
        }

        /// <summary>
        /// Pobiera dostępne serwery.
        /// </summary>
        public void GetPlayableServers()
        {
            //hubProxy.Invoke("GetPlayableServers");
            int count = 0;
            while (!hubProxy.Invoke("GetPlayableServers", guid).Wait(1000))
            {
                count++;
                if (count == reconnectDelay)
                {
                    Dispose();
                    Connect(username, guid, isReverseGame);
                }
            }
        }

        /// <summary>
        /// Dołączanie do gry.
        /// </summary>
        public void JoinGame(Game game, Player player)
        {
            //hubProxy.Invoke("JoinGame", game, player).Wait(1000)
            int count = 0;
            while (!hubProxy.Invoke("JoinGame", guid, game, player).Wait(1000))
            {
                count++;
                if (count == reconnectDelay)
                {
                    Dispose();
                    Connect(username, guid, isReverseGame);
                }
            }
        }

        /// <summary>
        /// Wykonywanie ruchu gracza.
        /// </summary>
        public void MakeMove(Game game, Player player, Position position)
        {
            //hubProxy.Invoke("MakeMove", game, player, position);
            int count = 0;
            while (!hubProxy.Invoke("MakeMove", guid, game, player, position).Wait(1000))
            {
                count++;
                if (count == reconnectDelay)
                {
                    Dispose();
                    Connect(username, guid, isReverseGame);
                }
            }
        }

        /// <summary>
        /// Sprawdza czy otrzymano sygnał anulowania gry.
        /// </summary>
        /// <returns>
        /// Wynik czy otrzymano anulowanie gry.
        /// </returns>
        public bool GetCancelGameResult()
        {
            SpinWait.SpinUntil(() => cancelGameCreationResultReceived);
            cancelGameCreationResultReceived = false;
            return cancelGameCreationResult;
        }

        /// <summary>
        /// Pobiera wynik stworzenia gry.
        /// </summary>
        /// <returns>
        /// Obiekt klasy Game.
        /// </returns>
        public Game GetCreateGameResult()
        {
            SpinWait.SpinUntil(() => createGameResultReceived);
            createGameResultReceived = false;
            return createGameResult;
        }

        /// <summary>
        /// Pobiera wynik dołączenia do gry.
        /// </summary>
        /// <returns>
        /// Obiekt klasy Game.
        /// </returns>
        public Game GetJoinGameResult()
        {
            SpinWait.SpinUntil(() => joinGameResultReceived);
            joinGameResultReceived = false;
            return joinGameResult;
        }

        /// <summary>
        /// Pobiera dołączonego gracza.
        /// </summary>
        /// <returns>
        /// Obiekt klasy Game.
        /// </returns>
        public Game GetJoinedPlayer()
        {
            SpinWait.SpinUntil(() => playerJoinedGameReceived);
            playerJoinedGameReceived = false;
            return playerJoinedGame;
        }

        /// <summary>
        /// Pobiera wynik dostępnych serwerów.
        /// </summary>
        /// <returns>
        /// Lista gier.
        /// </returns>
        public IList<Game> GetPlayableServersResult()
        {
            SpinWait.SpinUntil(() => playableServersResultReceived);
            playableServersResultReceived = false;
            return playableServersResult;
        }

        /// <summary>
        /// Sprawdza czy został wykonany ruch.
        /// </summary>
        /// <returns>
        /// Wynik czy został wykonany ruch.
        /// </returns>
        public MoveResult GetMakeMoveResult()
        {
            SpinWait.SpinUntil(() => makeMoveResultReceived);
            makeMoveResultReceived = false;
            return makeMoveResult;
        }

        /// <summary>
        /// Pobiera wynik ruchu gracza.
        /// </summary>
        /// <returns>
        /// Obiekt klasy MoveResult.
        /// </returns>
        public MoveResult GetPlayerMadeMoveResult()
        {
            SpinWait.SpinUntil(() => playerMadeMoveResultReceived);
            playerMadeMoveResultReceived = false;
            return playerMadeMoveResult;
        }
    }
}
