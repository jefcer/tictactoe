﻿using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Interfejs fabryki.
    /// </summary>
    public interface IBoardFactory
    {
        IBoard GetNormalBoard();
        IBoard GetBigBoard();
        IBoard GetLargeBoard();
    }
}
