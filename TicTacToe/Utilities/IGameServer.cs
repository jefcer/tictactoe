﻿using System;
using System.Collections.Generic;
using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Interfejs klasy GameServer.
    /// </summary>
    public interface IGameServer : IDisposable
    {
        void CancelGame(Game game);
        void CreateGame(Game game);
        void GetPlayableServers();
        void JoinGame(Game game, Player player);
        void MakeMove(Game game, Player player, Position position);

        bool GetCancelGameResult();
        Game GetCreateGameResult();
        IList<Game> GetPlayableServersResult();
        Game GetJoinGameResult();
        Game GetJoinedPlayer();
        MoveResult GetMakeMoveResult();
        MoveResult GetPlayerMadeMoveResult();
    }
}
