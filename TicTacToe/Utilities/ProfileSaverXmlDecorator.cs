﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Klasa rozszerzająca zapisywanie profilu w formacie Xml.
    /// </summary>
    public class ProfileSaverXmlDecorator : ProfileSaverDecorator
    {
        public ProfileSaverXmlDecorator(IProfileSaver profileSaver) : base(profileSaver)
        {

        }

        /// <summary>
        /// Zapisywanie profilu.
        /// </summary>
        public override void Save(string filePath)
        {
            File.WriteAllText(filePath, ParseToFile());
        }

        /// <summary>
        /// Zamienia ustawienia profilu na stringa.
        /// </summary>
        /// <returns>
        /// Ustawienia profilu jako string.
        /// </returns>
        public override string ParseToFile()
        {
            var fileToDecorate = base.ParseToFile();
            var lines = fileToDecorate.Split('\n');

            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("<Profile>");
            stringBuilder.AppendLine("\t<Username>");
            stringBuilder.AppendLine("\t\t" + lines[0].Replace("\r", ""));
            stringBuilder.AppendLine("\t</Username>");
            stringBuilder.AppendLine("</Profile>");
            stringBuilder.AppendLine("<ProgramSettings>");
            stringBuilder.AppendLine("\t<BackgroundColor>");
            stringBuilder.AppendLine("\t\t" + lines[2].Replace("\r", ""));
            stringBuilder.AppendLine("\t</BackgroundColor>");
            stringBuilder.AppendLine("\t<MusicVolume>");
            stringBuilder.AppendLine("\t\t" + lines[3].Replace("\r", ""));
            stringBuilder.AppendLine("\t</MusicVolume>");
            stringBuilder.AppendLine("\t<PawnColorO>");
            stringBuilder.AppendLine("\t\t" + lines[4].Replace("\r", ""));
            stringBuilder.AppendLine("\t</PawnColorO>");
            stringBuilder.AppendLine("\t<PawnColorX>");
            stringBuilder.AppendLine("\t\t" + lines[5].Replace("\r", ""));
            stringBuilder.AppendLine("\t</PawnColorX>");
            stringBuilder.AppendLine("\t<SoundVolume>");
            stringBuilder.AppendLine("\t\t" + lines[6].Replace("\r", ""));
            stringBuilder.AppendLine("\t</SoundVolume>");
            stringBuilder.AppendLine("</ProgramSettings>");

            return stringBuilder.ToString();
        }
    }
}
