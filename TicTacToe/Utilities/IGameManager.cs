﻿using System.Collections.Generic;
using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Interfejs menadżera gry.
    /// </summary>
    public interface IGameManager
    {
        bool Move(ref Game game, ref Player player, ref Position position);
        void GetOtherPlayerMove(ref Game game, ref Position position);
        void Join(ref Game game, ref Player player);
        bool CancelGameCreation(Game game);
        void AddGame(ref Game game);
        IList<Game> FindPlayableGames();
    }
}
