﻿using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Klasa przechowująca globalne zmienne.
    /// </summary>
    public static class StaticHelper
    {
        public static Player ActivePlayer;
        public static IGameManager GameManager;
        public static Game Game;
    }
}
