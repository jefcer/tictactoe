﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media;
using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Menadżer profili
    /// </summary>
    public class ProfilesManager
    {
        private string filePath = "profiles.txt"; // TODO: load it from settings

        /// <summary>
        /// Wczytuje graczy.
        /// </summary>
        /// <returns>
        /// Lista graczy.
        /// </returns>
        public List<Player> LoadPlayers()
        {
            var fileContent = File.Exists(filePath) ? File.ReadAllText(filePath) : "";
            return ParseFromFile(fileContent);
        }

        /// <summary>
        /// Pobieranie z pliku wszystkich graczy.
        /// </summary>
        /// <returns>
        /// Lista graczy.
        /// </returns>
        private List<Player> ParseFromFile(string fileContent)
        {
            var lines = fileContent.Split('\n');
            var result = new List<Player>();

            for (var i = 0; i < lines.Length - 1; i += 7)
            {
                Int32.TryParse(lines[i + 1].Trim(), out int wins);
                result.Add(new Player { Username = lines[i].Trim(), Wins = wins, Guid = Guid.NewGuid().ToString() });
            }

            return result;
        }

        /// <summary>
        /// Pobieranie ustawień gracza.
        /// </summary>
        public void GetPlayerSettings(string username)
        {
            var fileContent = File.ReadAllText(filePath);
            var lines = fileContent.Split('\n');

            for (var i = 0; i < lines.Length - 1; i += 7)
            {
                if (lines[i].Trim() == username)
                {
                    var instance = ProgramSettings.Instance;
                    var color = lines[i + 2].Trim();
                    instance.BackgroundColor = (Color)ColorConverter.ConvertFromString(color);
                    Int32.TryParse(lines[i + 3].Trim(), out int musicVolume);
                    instance.MusicVolume = musicVolume;
                    color = lines[i + 4].Trim();
                    instance.PawnColorO = (Color)ColorConverter.ConvertFromString(color);
                    color = lines[i + 5].Trim();
                    instance.PawnColorX = (Color)ColorConverter.ConvertFromString(color);
                    Int32.TryParse(lines[i + 6].Trim(), out int soundVolume);
                    instance.SoundVolume = soundVolume;
                    return;
                }
            }

            throw new Exception("Can't find settings for this user");
        }

        /// <summary>
        /// Dodawanie profilu użytkownika.
        /// </summary>
        public void AddProfile(Profile profile)
        {
            var fileContent = File.Exists(filePath) ? File.ReadAllText(filePath) : "";

            if (CheckIfExists(profile.Player.Username, fileContent))
            {
                throw new Exception("There is already a profile with same username");
            }

            IProfileSaver profileSaver = new ProfileSaver(profile);
            var profileContent = profileSaver.ParseToFile();
            fileContent += profileContent;
            File.WriteAllText(filePath, fileContent);
        }

        /// <summary>
        /// Sprawdza czy istnieje taki profil.
        /// </summary>
        /// <returns>
        /// Wynik czy profil istnieje.
        /// </returns>
        private bool CheckIfExists(string username, string fileContent)
        {
            var lines = fileContent.Split('\n');

            for (var i = 0; i < lines.Length - 1; i += 7)
            {
                if (lines[i].Trim() == username)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Usuwanie profilu użytkownika.
        /// </summary>
        public void DeleteProfile(string username)
        {
            var fileContent = File.Exists(filePath) ? File.ReadAllText(filePath) : "";
            var lines = fileContent.Split('\n');

            for (var i = 0; i < lines.Length - 1; i += 7)
            {
                if (lines[i].Trim() == username)
                {
                    var linesTmp = lines.ToList();
                    linesTmp.RemoveRange(i, 7);
                    lines = linesTmp.ToArray();

                    var tmp = string.Join("", lines).Replace("\r", Environment.NewLine);
                    File.WriteAllText(filePath, tmp);

                    return;
                }
            }

            throw new Exception("Cannot find given profile");
        }

        /// <summary>
        /// Uaktualnia profil użytkownika.
        /// </summary>
        public void UpdateProfile(Profile profile)
        {
            var fileContent = File.Exists(filePath) ? File.ReadAllText(filePath) : "";
            var lines = fileContent.Split('\n');

            for (var i = 0; i < lines.Length - 1; i += 7)
            {
                if (lines[i].Trim() == profile.Player.Username)
                {
                    IProfileSaver profileSaver = new ProfileSaver(profile);
                    var profileContent = profileSaver.ParseToFile().Split('\n');

                    lines[i + 1] = profileContent[1];
                    lines[i + 2] = profileContent[2];
                    lines[i + 3] = profileContent[3];
                    lines[i + 4] = profileContent[4];
                    lines[i + 5] = profileContent[5];
                    lines[i + 6] = profileContent[6];

                    var tmp = string.Join("", lines).Replace("\r", Environment.NewLine);
                    File.WriteAllText(filePath, tmp);

                    return;
                }
            }

            throw new Exception("Cannot find given profile");
        }
    }
}
