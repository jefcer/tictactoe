﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Klasa rozszerzająca zapisywanie profilu w formacie Json.
    /// </summary>
    public class ProfileSaverJsonDecorator : ProfileSaverDecorator
    {
        public ProfileSaverJsonDecorator(IProfileSaver profileSaver) : base(profileSaver)
        {

        }

        /// <summary>
        /// Zapisywanie profilu.
        /// </summary>
        public override void Save(string filePath)
        {
            File.WriteAllText(filePath, ParseToFile());
        }

        /// <summary>
        /// Zamienia ustawienia profilu na stringa.
        /// </summary>
        /// <returns>
        /// Ustawienia profilu jako string.
        /// </returns>
        public override string ParseToFile()
        {
            var fileToDecorate = base.ParseToFile();
            var lines = fileToDecorate.Split('\n');

            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("{");
            stringBuilder.AppendLine("\t\"Profile\": { ");
            stringBuilder.AppendLine("\t\t\"Username\": \"" + lines[0].Replace("\r", "") + "\"");
            stringBuilder.AppendLine("\t},");
            stringBuilder.AppendLine("\t\"ProgramSettings\": { ");
            stringBuilder.AppendLine("\t\t\"BackgroundColor\": \"" + lines[2].Replace("\r", "") + "\",");
            stringBuilder.AppendLine("\t\t\"MusicVolume\": \"" + lines[3].Replace("\r", "") + "\",");
            stringBuilder.AppendLine("\t\t\"PawnColorO\": \"" + lines[4].Replace("\r", "") + "\",");
            stringBuilder.AppendLine("\t\t\"PawnColorX\": \"" + lines[5].Replace("\r", "") + "\",");
            stringBuilder.AppendLine("\t\t\"SoundVolume\": \"" + lines[6].Replace("\r", "") + "\"");
            stringBuilder.AppendLine("\t}");
            stringBuilder.AppendLine("}");

            return stringBuilder.ToString();
        }
    }
}
