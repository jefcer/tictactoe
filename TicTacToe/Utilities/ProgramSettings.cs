﻿using System.Text;
using System.Windows.Media;
using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Wzorzec singleton.
    /// </summary>
    public class ProgramSettings
    {
        public int SoundVolume { get; set; }
        public int MusicVolume { get; set; }
        public Color BackgroundColor { get; set; }
        public Color PawnColorX { get; set; }
        public Color PawnColorO { get; set; }

        private static ProgramSettings instance = new ProgramSettings();

        /// <summary>
        /// Pobiera ustawienia profilu użytkownika.
        /// </summary>
        /// <returns>
        /// Instancja klasy ProgramSettings - czyli ustawienia profilu użytkownika.
        /// </returns>
        public static ProgramSettings Instance { get { return instance; } }


        /// <summary>
        /// Prywatny konstruktor klasy ProgramSettings.
        /// </summary>
        private ProgramSettings()
        {
            ResetSettings();
        }

        /// <summary>
        /// Resetuje ustawienia profilu użytkownika.
        /// </summary>
        public void ResetSettings()
        {
            SoundVolume = 50;
            MusicVolume = 50;
            BackgroundColor = Color.FromRgb(0, 0, 0);
            PawnColorX = Color.FromRgb(255, 0, 0);
            PawnColorO = Color.FromRgb(0, 0, 255);
        }
    }
}
