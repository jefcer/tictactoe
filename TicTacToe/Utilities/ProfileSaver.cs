using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Klasa do zapisywania profilu użytkownika.
    /// </summary>
    public class ProfileSaver : IProfileSaver
    {
        protected Profile profile;

        /// <summary>
        /// Konstruktor klasy ProfileSaver
        /// </summary>
        public ProfileSaver(Profile profile)
        {
            this.profile = profile;
        }

        /// <summary>
        /// Zapisuje ustawienia profilu do pliku.
        /// </summary>
        public void Save(string filePath)
        {
            File.WriteAllText(filePath, ParseToFile());
        }

        /// <summary>
        /// Zamienia ustawienia profilu na stringa.
        /// </summary>
        /// <returns>
        /// Ustawienia profilu jako string.
        /// </returns>
        public string ParseToFile()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(profile.Player.Username);
            stringBuilder.AppendLine(profile.Player.Wins.ToString());
            stringBuilder.AppendLine(profile.ProgramSettings.BackgroundColor.ToString());
            stringBuilder.AppendLine(profile.ProgramSettings.MusicVolume.ToString());
            stringBuilder.AppendLine(profile.ProgramSettings.PawnColorO.ToString());
            stringBuilder.AppendLine(profile.ProgramSettings.PawnColorX.ToString());
            stringBuilder.AppendLine(profile.ProgramSettings.SoundVolume.ToString());
            return stringBuilder.ToString();
        }
    }
}
