﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Wzorzec Dekorator
    /// </summary>
    public abstract class ProfileSaverDecorator : IProfileSaver
    {
        IProfileSaver profileSaver;

        /// <summary>
        /// Konstruktor klasy ProfileSaverDecorator.
        /// </summary>
        public ProfileSaverDecorator(IProfileSaver profileSaver)
        {
            this.profileSaver = profileSaver;
        }

        /// <summary>
        /// Zapisywanie profilu.
        /// </summary>
        public virtual void Save(String filePath)
        {
            profileSaver.Save(filePath);
        }

        /// <summary>
        /// Zamienia ustawienia profilu na stringa.
        /// </summary>
        /// <returns>
        /// Ustawienia profilu jako string.
        /// </returns>
        public virtual string ParseToFile()
        {
            return profileSaver.ParseToFile();
        }
    }
}
