﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Interface;

namespace TicTacToe.Utilities
{
    /// <summary>
    /// Wzorzec Adapter
    /// </summary>
    public class GameManagerAdapter : IGameManager // TODO: throw exceptions in case of failure
    {
        private IGameServer gameServer;

        public GameManagerAdapter(IGameServer gameServer)
        {
            this.gameServer = gameServer;
        }

        /// <summary>
        /// Tworzenie gry.
        /// </summary>
        public void AddGame(ref Game game)
        {
            gameServer.CreateGame(game);
            game = gameServer.GetCreateGameResult();
            if (game == null)
            {
                // TODO: throw exception with game creation error
            }
            game = gameServer.GetJoinedPlayer();
            if (game == null)
            {
                // TODO: throw exception with player join error
            }
            // return game;
        }

        /// <summary>
        /// Anulowanie tworzenia gry.
        /// </summary>
        /// <returns>
        /// Wynik czy anulowano tworzenie gry.
        /// </returns>
        public bool CancelGameCreation(Game game)
        {
            gameServer.CancelGame(game);
            var result = gameServer.GetCancelGameResult();
            return result;
        }

        /// <summary>
        /// Pobiera dostępnych graczy.
        /// </summary>
        /// <returns>
        /// Lista dostępnych graczy.
        /// </returns>
        public IList<Game> FindPlayableGames()
        {
            gameServer.GetPlayableServers();
            var result = gameServer.GetPlayableServersResult();
            return result;
        }

        /// <summary>
        /// Dołączanie do gry.
        /// </summary>
        public void Join(ref Game game, ref Player player)
        {
            gameServer.JoinGame(game, player);
            var result = gameServer.GetJoinGameResult();
            game = result;
            player = result.Guest.Value;
            //return result;
        }

        /// <summary>
        /// Wykonanie ruchu gracza.
        /// </summary>
        /// <returns>
        /// Wynik czy udało się wykonać ruch.  
        /// </returns>
        public bool Move(ref Game game, ref Player player, ref Position position)
        {
            var oldResult = game.GameResult;
            gameServer.MakeMove(game, player, position);
            var result = gameServer.GetMakeMoveResult();
            game = result.Game;
            player = result.Player;
            position = result.Position;
            return oldResult != game.GameResult;
        }

        /// <summary>
        /// Pobiera współrzędne gracza.
        /// </summary>
        public void GetOtherPlayerMove(ref Game game, ref Position position)
        {
            var result = gameServer.GetPlayerMadeMoveResult();
            game = result.Game;
            position = result.Position;
            //return result;
        }
    }
}
