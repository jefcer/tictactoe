﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TicTacToe.Interface;
using TicTacToe.Utilities;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for CreateUserName.xaml
    /// </summary>
    public partial class CreateUserName : Window
    {
        public CreateUserName()
        {
            InitializeComponent();
            Color c = ProgramSettings.Instance.BackgroundColor;
            Background = new SolidColorBrush(c);
        }

        private void CreateUserNameBtn_Click(object sender, RoutedEventArgs e)
        {
            string pattern = @"^[a-z0-9]+$";
            Regex regex = new Regex(pattern);
            if (UserNameTxt.Text.Length < 4)
            {
                MessageBoxResult result = MessageBox.Show("Username must be at least 4 characters long", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (!regex.IsMatch(UserNameTxt.Text))
            {
                MessageBoxResult result = MessageBox.Show("Username must consist only of small letters and numbers", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                ProfilesManager profilesManager = new ProfilesManager();
                ProgramSettings.Instance.ResetSettings();
                var profile = new Profile { Player = new Player { Username = UserNameTxt.Text, Wins = 0 }, ProgramSettings = ProgramSettings.Instance };
                profilesManager.AddProfile(profile);
                Close();
            }
        }
    }
}
