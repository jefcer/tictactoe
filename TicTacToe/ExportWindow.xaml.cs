﻿using Microsoft.Win32;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using TicTacToe.Utilities;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for ExportWindow.xaml
    /// </summary>
    public partial class ExportWindow : Window
    {
        private Profile profile;

        public ExportWindow(Profile profile)
        {
            InitializeComponent();

            this.profile = profile;
        }

        private void JsonBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "JSON file (*.json)|*.json";
            if (saveFileDialog.ShowDialog() == true)
            {
                IProfileSaver profileSaver = new ProfileSaverJsonDecorator(new ProfileSaver(profile));
                profileSaver.Save(saveFileDialog.FileName);
                Close();
            }
        }

        private void XmlBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XML file (*.xml)|*.xml";
            if (saveFileDialog.ShowDialog() == true)
            {
                IProfileSaver profileSaver = new ProfileSaverXmlDecorator(new ProfileSaver(profile));
                profileSaver.Save(saveFileDialog.FileName);
                Close();
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
