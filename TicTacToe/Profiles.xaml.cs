﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TicTacToe.Interface;
using TicTacToe.Utilities;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for Profiles.xaml
    /// </summary>
    public partial class Profiles : Window
    {
        public static bool IsReverseGame = false;
        private ProfilesManager profilesManager = new ProfilesManager();
        private List<Player> players;

        public Profiles()
        {
            InitializeComponent();
            Color c = ProgramSettings.Instance.BackgroundColor;
            Background = new SolidColorBrush(c);

            LoadProfiles();
        }

        private void LoadProfiles()
        {
            players = profilesManager.LoadPlayers();

            if (players.Count == 0)
            {
                var createUserName = new CreateUserName();
                createUserName.ShowDialog();
                players = profilesManager.LoadPlayers();
            }

            ProfilesListBox.Items.Clear();

            foreach (var player in players)
            {
                ProfilesListBox.Items.Add(player.Username);
            }
        }

        private void SelectBtn_Click(object sender, RoutedEventArgs e)
        {
            IsReverseGame = ReverseGameCheckbox.IsChecked.Value;
            string username = (string)ProfilesListBox.SelectedItem;
            profilesManager.GetPlayerSettings(username);
            StaticHelper.ActivePlayer = players.Where(p => p.Username == username).First();

            DialogResult = true;
            Close();
        }

        private void CreateBtn_Click(object sender, RoutedEventArgs e)
        {
            var createUserName = new CreateUserName();
            createUserName.ShowDialog();
            LoadProfiles();
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (players.Count == 1)
            {
                MessageBoxResult result = MessageBox.Show("You can not delete the only profile", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                string username = (string)ProfilesListBox.SelectedItem;
                profilesManager.DeleteProfile(username);
                ProfilesListBox.Items.Remove(username);
            }
        }

        private void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            string username = (string)ProfilesListBox.SelectedItem;
            profilesManager.GetPlayerSettings(username);
            StaticHelper.ActivePlayer = players.Where(p => p.Username == username).First();
            var exportWindow = new ExportWindow(new Profile { Player = StaticHelper.ActivePlayer, ProgramSettings = ProgramSettings.Instance });
            exportWindow.ShowDialog();
        }
    }
}
