﻿using System;
using System.Windows;
using System.Windows.Media;
using TicTacToe.Utilities;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for GameMenu.xaml
    /// </summary>
    public partial class GameMenu : Window
    {
        private IGameServer gameServer;

        public GameMenu()
        {
            InitializeComponent();

            var profiles = new Profiles();
            if (!profiles.ShowDialog().Value)
            {
                Close();
            }

            UserNameLbl.Content = string.Format("Hi {0}!", StaticHelper.ActivePlayer.Username);

            StaticHelper.ActivePlayer.Guid = Guid.NewGuid().ToString();

            var connectingWindow = new InfoWindow("Connecting to the server...");
            connectingWindow.ContentRendered += (ea, a) =>
            {
                try
                {
                    gameServer = new GameServer(StaticHelper.ActivePlayer.Username, StaticHelper.ActivePlayer.Guid, Profiles.IsReverseGame);
                    StaticHelper.GameManager = new GameManagerAdapter(gameServer);
                }
                catch (AggregateException)
                {
                    MessageBoxResult result = MessageBox.Show("Couldn't connect to the server\nTry to restart your application", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    Close();
                }
                finally
                {
                    connectingWindow.Close();
                }
            };
            connectingWindow.ShowDialog();

            Color c = ProgramSettings.Instance.BackgroundColor;
            Background = new SolidColorBrush(c);
        }

        private void NewGameBtn_Click(object sender, RoutedEventArgs e)
        {
            var newGame = new NewGame();
            newGame.Closed += (ea, a) => Show();
            newGame.Show();
            Hide();
        }

        private void SettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            var settings = new Settings();
            settings.Closed += (ea, a) => Show();
            settings.Show();
            Hide();
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gameServer != null)
            {
                gameServer.Dispose();
            }
        }
    }
}
