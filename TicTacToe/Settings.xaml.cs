﻿using System.Windows;
using System.Windows.Media;
using TicTacToe.Utilities;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        private ProfilesManager profilesManager = new ProfilesManager();

        public Settings()
        {
            InitializeComponent();
            Color c = ProgramSettings.Instance.BackgroundColor;
            Background = new SolidColorBrush(c);

            SoundSlider.Value = ProgramSettings.Instance.SoundVolume;
            MusicSlider.Value = ProgramSettings.Instance.MusicVolume;
            BackgroundColorPicker.SelectedColor = ProgramSettings.Instance.BackgroundColor;
            PawnColorX.SelectedColor = ProgramSettings.Instance.PawnColorX;
            PawnColorO.SelectedColor = ProgramSettings.Instance.PawnColorO;
        }

        private void SaveSettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            ProgramSettings.Instance.SoundVolume = (int)SoundSlider.Value;
            ProgramSettings.Instance.MusicVolume = (int)MusicSlider.Value;
            if (BackgroundColorPicker.SelectedColor.HasValue)
            {
                ProgramSettings.Instance.BackgroundColor = BackgroundColorPicker.SelectedColor.Value;
                UpdateBackGroundColors();
            }
            if (PawnColorX.SelectedColor.HasValue)
            {
                ProgramSettings.Instance.PawnColorX = PawnColorX.SelectedColor.Value;
            }
            if (PawnColorO.SelectedColor.HasValue)
            {
                ProgramSettings.Instance.PawnColorO = PawnColorO.SelectedColor.Value;
            }

            

            profilesManager.UpdateProfile(new Profile { Player = StaticHelper.ActivePlayer, ProgramSettings = ProgramSettings.Instance });
            Close();
        }
        public void UpdateBackGroundColors()
        {
            foreach (Window window in Application.Current.Windows)
            {
                Color c = ProgramSettings.Instance.BackgroundColor;
                window.Background = new SolidColorBrush(c);
            }
        }
    }

        
}
