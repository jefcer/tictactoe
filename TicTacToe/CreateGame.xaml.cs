﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media;
using TicTacToe.Interface;
using TicTacToe.Utilities;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for CreateGame.xaml
    /// </summary>
    public partial class CreateGame : Window
    {
        public CreateGame()
        {
            InitializeComponent();
            Color c = ProgramSettings.Instance.BackgroundColor;
            Background = new SolidColorBrush(c);
        }

        private void CreateGameBtn_Click(object sender, RoutedEventArgs e)
        {
            int minimumWins;
            Int32.TryParse(MinWindTxtbox.Text, out minimumWins);

            IBoard board;
            if (SizeBoardRbtn3.IsChecked.Value)
            {
                board = new NormalBoard();
            }
            else if (SizeBoardRbtn4.IsChecked.Value)
            {
                board = new BigBoard();
            }
            else
            {
                board = new LargeBoard();
            }

            var builder = new GameBuilder()
                .UseRounds(RoundsRbtn3.IsChecked.Value ? 3
                    : RoundsRbtn5.IsChecked.Value ? 5
                    : 9)
                .UseMoveTimeLimit((int)MoveTimeSlider.Value)
                .UseStartingPawn(NoughtRbtn.IsChecked.Value ? StartPawnType.Nought : StartPawnType.Cross)
                .UseMinimumWins(minimumWins)
                .UseDrawCountsAsWin(CountsAsWinChbox.IsChecked.Value)
                .UseBoard(board);

            StaticHelper.Game = new Game(builder);
            StaticHelper.Game.Host = StaticHelper.ActivePlayer;

            DialogResult = true;
            Close();
        }

        private void MinWindTxtbox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            if (regex.IsMatch(e.Text))
            {
                int value;
                if (Int32.TryParse(e.Text, out value))
                {
                    if (value < 0)
                    {
                        value = 0;
                    }
                    else if (value > 100)
                    {
                        value = 100;
                    }

                    e.Handled = true;
                    return;
                }
            }

            e.Handled = false;
        }
    }
}
